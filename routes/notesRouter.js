const express = require('express');
// eslint-disable-next-line new-cap
const notesRouter = express.Router();
const {getUserNotes,
  addNote,
  getNote,
  updateNote,
  checkOrUncheckNote,
  deleteNote} = require('../controllers/controllers');

notesRouter.route('/').get(getUserNotes).post(addNote);
notesRouter.route('/:id')
    .get(getNote)
    .put(updateNote)
    .patch(checkOrUncheckNote)
    .delete(deleteNote);

module.exports = notesRouter;
