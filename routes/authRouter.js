const express = require('express');
// eslint-disable-next-line new-cap
const authRouter = express.Router();
const {registerUser, loginUser} = require('../controllers/controllers');

authRouter.route('/register').post(registerUser);
authRouter.route('/login').post(loginUser);

module.exports = authRouter;
