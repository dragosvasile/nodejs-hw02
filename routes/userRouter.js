const express = require('express');
// eslint-disable-next-line new-cap
const userRouter = express.Router();
const {getUserInfo,
  deleteUser,
  changeUserPass} = require('../controllers/controllers');

userRouter.route('/me').get(getUserInfo);
userRouter.route('/me').delete(deleteUser);
userRouter.route('/me').patch(changeUserPass);

module.exports = userRouter;
