const bcrypt = require('bcryptjs');
const User = require('../models/user');
const Note = require('../models/note');
const jwt = require('jsonwebtoken');
const {TOKEN_KEY} = process.env;
const CustomErr = require('../utils/err');

const registerUser = async (req, res, next) => {
  try {
    const {username, password} = req.body;
    if (!(username && password)) {
      return next(new CustomErr(`
      The following fields are required: 'username', 'password'`, 400));
    }

    const oldUser = await User.findOne({username});
    if (oldUser) {
      return next(new CustomErr('User already exists.', 400));
    }
    // pass encryption
    const saltRounds = await bcrypt.genSalt(10);
    const encPass = await bcrypt.hash(password, saltRounds);

    const createdDate = new Date();

    // save new  user in db
    const user = await User.create({
      username,
      password: encPass,
      createdDate: createdDate,
    });

    const token = jwt.sign({
      userId: user._id,
      username, createdDate},
    TOKEN_KEY, {
      expiresIn: '2h',
    });
    user.token = token;
    return res.status(200).json({message: 'Success'});
  } catch (error) {
    return next(error);
  }
};

const loginUser = async (req, res, next) => {
  try {
    const {username, password} = req.body;
    if (!(username && password)) {
      return next(new CustomErr(`
      The following fields are required: 'username', 'password'`, 400));
    }

    const user = await User.findOne({username});

    if (user && (await bcrypt.compare(password, user.password))) {
      const createdDate = user.createdDate;
      const token = jwt.sign({
        userId: user._id,
        username, createdDate},
      TOKEN_KEY, {
        expiresIn: '2h',
      });
      user.token = token;
      return res.status(200).json({message: 'Success', jwt_token: token});
    } else {
      return next(new CustomErr('Username and/or password is invalid.', 400));
    }
  } catch (error) {
    return next(error);
  }
};

const getUserInfo = (req, res, next) => {
  try {
    const {userId, username, createdDate} = req.user;
    return res.status(200).json({
      user: {_id: userId,
        username: username,
        createdDate: createdDate,
      }});
  } catch (error) {
    return next(error);
  }
};

const deleteUser = async (req, res, next) => {
  try {
    const {userId, username, createdDate} = req.user;
    const deletedUser = await User.deleteOne({_id: userId});

    const token = jwt.sign({
      userId,
      username, createdDate},
    TOKEN_KEY, {
      expiresIn: '0h',
    });
    deletedUser.token = token;

    if (deletedUser.deletedCount === 0) {
      return next(new CustomErr('User does not exist', 400));
    }
    return res.status(200).json({message: 'Sucess'});
  } catch (error) {
    return next(error);
  }
};

const changeUserPass = async (req, res, next) => {
  try {
    const {userId} = req.user;
    const {oldPassword, newPassword} = req.body;
    const oldUser = await User.findOne({_id: userId});

    if (oldUser && (await bcrypt.compare(oldPassword, oldUser.password))) {
      const saltRounds = await bcrypt.genSalt(10);
      const encNewPass = await bcrypt.hash(newPassword, saltRounds);
      await User.updateOne(
          {_id: userId},
          {password: encNewPass});
      return res.status(200).json({message: 'Success'});
    } else {
      return next(new CustomErr('Old password does not match', 400));
    }
  } catch (error) {
    return next(error);
  }
};

const getUserNotes = async (req, res, next) => {
  const {offset = 0, limit = 0} = req.query;
  try {
    const {userId} = req.user;
    const userNotes = await Note.find(
        {userId: userId})
        .limit(limit)
        .skip(offset);
    const count = await Note.count();
    return res.status(200).json({offset, limit, count, notes: userNotes});
  } catch (error) {
    return next(error);
  }
};

const addNote = async (req, res, next) => {
  try {
    const {userId} = req.user;
    const {text} = req.body;
    if (!text) {
      return next(new CustomErr(`
      The following field is required: 'text'`, 400));
    }
    await Note.create({
      userId,
      completed: false,
      text: text,
      createdDate: new Date(),
    });
    return res.status(200).json({message: 'Success'});
  } catch (error) {
    return next(error);
  }
};

const getNote = async (req, res, next) => {
  try {
    const {userId} = req.user;
    const {id} = req.params;
    const note = await Note.findOne({_id: id, userId});
    if (note === null) {
      return next(new CustomErr('Note not found', 400));
    }
    return res.status(200).json({note});
  } catch (error) {
    return next(error);
  }
};

const updateNote = async (req, res, next) => {
  try {
    const {userId} = req.user;
    const {id} = req.params;
    const {text} = req.body;
    if (!text) {
      return next(new CustomErr(`
      The following field is required: 'text'`, 400));
    }
    const note = await Note.findOneAndUpdate({_id: id, userId}, {text});
    if (note === null) {
      return next(new CustomErr('Note not found', 400));
    }
    return res.status(200).json({message: 'Success'});
  } catch (error) {
    return next(error);
  }
};

const checkOrUncheckNote = async (req, res, next) => {
  try {
    const {userId} = req.user;
    const {id} = req.params;
    const note = await Note.findOne({_id: id, userId});
    if (note === null) {
      return next(new CustomErr('Note not found', 400));
    }
    const isChecked = !note.completed;
    await Note.findOneAndUpdate({_id: id, userId}, {completed: isChecked});
    return res.status(200).json({message: 'Success'});
  } catch (error) {
    return next(error);
  }
};

const deleteNote = async (req, res, next) => {
  try {
    const {userId} = req.user;
    const {id} = req.params;
    const note = await Note.findOneAndDelete({_id: id, userId});
    if (note === null) {
      return next(new CustomErr('Note not found', 400));
    }
    return res.status(200).json({message: 'Success'});
  } catch (error) {
    return next(error);
  }
};

module.exports = {registerUser,
  loginUser,
  getUserInfo,
  deleteUser,
  changeUserPass,
  getUserNotes,
  addNote,
  getNote,
  updateNote,
  checkOrUncheckNote,
  deleteNote,
};
