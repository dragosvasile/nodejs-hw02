require('dotenv').config();
require('./configs/database').connect();
const express = require('express');
const app = express();

const authRouter = require('./routes/authRouter');
const userRouter = require('./routes/userRouter');
const notesRouter = require('./routes/notesRouter');

const {errHandler} = require('./middlewares/err-handler');
const auth = require('./middlewares/auth');
const {PORT} = process.env;

app.use(express.json());

// auth router
app.use('/api/auth', authRouter);

// user profile router
app.use('/api/users', auth, userRouter);

// notes router
app.use('/api/notes', auth, notesRouter);

// error middleware
app.use(errHandler);

app.listen(PORT, (req, res) => {
  console.log(`Server started on port ${PORT}`);
});
