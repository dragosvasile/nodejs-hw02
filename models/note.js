const mongoose = require('mongoose');

const noteSchema = new mongoose.Schema({
  userId: String,
  completed: Boolean,
  text: String,
  createdDate: String,
});

const noteModel = mongoose.model('note', noteSchema);

module.exports = noteModel;
